import { dataGlasses } from "./main.js"; 

const renderGlasses = (dataGlasses) => {
  let content = ``;
  dataGlasses.map((item) => {
    content += `
        <image class="vglasses__items col-4" src="${item.src}" id="${item.id}" onclick="thaydoiKinh('${item.id}')"></image>
    `;
  });
  document.getElementById(`vglassesList`).innerHTML = content;
};
renderGlasses(dataGlasses);

const thaydoiKinh = (id) => {
  let thongTinKinh = ``;
  dataGlasses.forEach((item) => {
    if (item.id === id) {
      thongTinKinh += `
      <div class="row p-4" id="display-attribute-2">
        <p class="title"> ${item.id} - ${item.name} - ${item.brand} ( ${item.color})</p>
         <div class="row">
            <p class="price col-4">$${item.price}</p>
            <p class="status col-4">Stocking</p>
          </div>
          <p class="desc"> ${item.description}</p>
      </div>
      `;
      document.getElementById(`avatar`).innerHTML = `
          <image id="display-attribute" src="${item.virtualImg}" id="${item.id}"></image>
         `;
    }
  });
  document.getElementById(`glassesInfo`).innerHTML = thongTinKinh;
};

const removeGlasses = (index) => {
  if (index) {
    document.getElementById(`display-attribute`).style.display = `flex`;
    document.getElementById(`display-attribute-2`).style.display = `block`;
  } else {
    document.getElementById(`display-attribute`).style.display = `none`;
    document.getElementById(`display-attribute-2`).style.display = `none`;
  }
};

window.removeGlasses = removeGlasses;
window.thaydoiKinh = thaydoiKinh;



